package Controlador;

import modelo.Bomba;
import modelo.Gasolina;
import vista.Vista;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {
    private Vista vista;
    private Bomba bomba;
    private Gasolina gasolina;
    private int capacidadActualBomba;

    public Controlador(Vista vista, Bomba bomba, Gasolina gasolina) {
        this.vista = vista;
        this.bomba = bomba;
        this.gasolina = gasolina;

        // Configure event listeners
        this.vista.btnIniciarBomba.addActionListener(this);
        this.vista.btnRegistrar2.addActionListener(this);
        this.capacidadActualBomba = vista.txtCapBomba.getValue();
    }

    private void iniciarVista() {
        vista.setTitle("::    Gasolinera   ::");
        vista.setSize(587, 430);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnIniciarBomba) {
            try {
                int numBomba = Integer.parseInt(vista.txtNumBomba.getText());
                String tipoGasolina = vista.txtTipoGasolina.getSelectedItem().toString();
                double precioVenta = 0.0;

                switch (tipoGasolina) {
                    case "Regular":
                        precioVenta = 19.5;
                        break;
                    case "Premium":
                        precioVenta = 23.0;
                        break;
                    case "Diesel":
                        precioVenta = 21.0;
                        break;
                }

                // Set the selling price in the text field
                vista.txtPrecioVenta.setText(String.valueOf(precioVenta));

                Gasolina gasolina = new Gasolina(1, tipoGasolina, precioVenta);
                bomba.iniciarBomba(numBomba, gasolina);

                // Lock the text fields
                vista.txtTipoGasolina.setEnabled(false);
                vista.txtNumBomba.setEnabled(false);

                // Initialize the sales counter to zero
                int contadorVentas = 0;
                vista.txtContador.setText(String.valueOf(contadorVentas));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "El número de bomba debe ser un valor entero.");
            }
        } else if (e.getSource() == vista.btnRegistrar2) {
            try {
                double cantidad = Double.parseDouble(vista.txtCantidad.getText());

                if (cantidad > capacidadActualBomba) {
                    JOptionPane.showMessageDialog(vista, "No hay suficiente gasolina en la bomba.");
                    return; // Exit the actionPerformed method without completing the sale
                }

                double costo = bomba.venderGasolina(cantidad);

                if (costo > 0) {
                    double ventasTotales = bomba.getVentasTotales();
                    vista.txtCosto.setText(String.valueOf(costo));
                    vista.txtTotalVentas.setText(String.valueOf(ventasTotales));

                    int capacidadBomba = vista.txtCapBomba.getValue();

                    if (capacidadBomba == 0) {
                        System.out.println("Capacidad de la bomba: " + capacidadBomba);
                        JOptionPane.showMessageDialog(vista, "¡Se ha agotado la gasolina!");
                    } else {
                        capacidadBomba = capacidadActualBomba - (int) cantidad;
                        vista.txtCapBomba.setValue(capacidadBomba);
                        capacidadActualBomba = capacidadBomba;

                        // Increment the sales counter
                        int contadorVentas = Integer.parseInt(vista.txtContador.getText());
                        contadorVentas++;
                        vista.txtContador.setText(String.valueOf(contadorVentas));
                    }
                } else {
                    JOptionPane.showMessageDialog(vista, "La cantidad ingresada no es válida");
                }
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "La cantidad debe ser un valor numérico.");
            }
        }
    }

    public static void main(String[] args) {
        Bomba bomba = new Bomba();
        Vista vista = new Vista(new JFrame(), true);
        Gasolina gasolina = new Gasolina(1, "Regular", 19.5); // Example gasolina

        Controlador controlador = new Controlador(vista, bomba, gasolina);
        controlador.iniciarVista();
    }
}

