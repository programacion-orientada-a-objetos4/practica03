package modelo;

public class Bomba {
    private int numBomba;
    private Gasolina gasolina;
    private double ventasTotales;

    public void iniciarBomba(int numBomba, Gasolina gasolina) {
        this.numBomba = numBomba;
        this.gasolina = gasolina;
        this.ventasTotales = 0;
    }

    public double venderGasolina(double cantidad) {
        if (cantidad <= gasolina.getCantidad()) {
            double costo = cantidad * gasolina.getPrecio();
            gasolina.setCantidad(gasolina.getCantidad() - cantidad);
            ventasTotales += costo;
            return costo;
        } else {
            return 0;
        }
    }

    public double getVentasTotales() {
        return ventasTotales;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }

    public double getTotalSales() {
        return ventasTotales;
    }

    public void setTotalSales(double ventasTotales) {
        this.ventasTotales = ventasTotales;
    }
}
